import PropTypes from 'prop-types'
import {Component} from 'react'
import R from 'ramda'
import {ShopTag} from 'components/GoogleMap'

const {Map, LatLng} = window.google.maps

class GoogleMap extends Component {

  shouldComponentUpdate() {
    // Every rerender logic should be handled by google maps API, to avoid unnecessary map reinitializaitons
    return false
  }

  componentDidMount() {
    // Since this component never rerender, we can set map static property
    this.map = new Map(this.refs.mapContainer, {
      zoom: 19,
      center: this.props.center
    })
  }

  componentWillReceiveProps(nextProps) {
    // Handle markers update
    if(!R.equals(this.props.markers, nextProps.markers)){
      R.forEach(marker => new ShopTag(marker, this.map), nextProps.markers)
    }

    // Handle map recentering
    if(!R.equals(this.props.center, nextProps.center)){
      this.map.setCenter(new LatLng(nextProps.center.lat, nextProps.center.lng))
    }
  }

  render() {
    return (
      <div ref="mapContainer" style={{width: '80%', height: '100vh'}} />
    )
  }
}

GoogleMap.propTypes = {
  markers: PropTypes.array.isRequired,
  center: PropTypes.object
}


export default GoogleMap
