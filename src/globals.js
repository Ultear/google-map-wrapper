import React from 'react'

// Shims
import entries from 'object.entries'

if(!Object.entries) {
  entries.shim()
}

Object.assign(global, {React})
