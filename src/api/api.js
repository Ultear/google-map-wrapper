import data from './db.json'
import {v4} from 'uuid'
import {reduce} from 'ramda'

export const getShops = () => new Promise((resolve) => {

  let daysOrder = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

  // Parsing API Data
  let shops = data.map(shop => Object.assign(shop, {
      id: v4(),
      hours: reduce((hours, day) => Object.assign({}, hours, {[day] : shop.hours[0][day]}), {}, daysOrder),
      longitude: Number(shop.longitude),
      latitude: Number(shop.latitude)
    })
  )

  // Fake async request
  setTimeout(() => {
    resolve(shops)
  }, 1000)
})
