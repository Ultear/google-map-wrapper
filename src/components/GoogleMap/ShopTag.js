import React from 'react'
import {renderToStaticMarkup} from 'react-dom/server'
import {Marker, Tooltip} from 'components/GoogleMap'
const {OverlayView, LatLng, InfoWindow} = window.google.maps

function ShopTag(marker, map) {
  this.marker = marker
  this.position = new LatLng(this.marker.latitude, this.marker.longitude)
  this.map = map
  this.div = null


  this.setMap(map)
}

ShopTag.prototype = new OverlayView()

ShopTag.prototype.onAdd = function() {
  let tempDiv = document.createElement('div')
  tempDiv.innerHTML = renderToStaticMarkup(React.createElement(Marker, {...this.marker}))

  this.div = tempDiv.firstChild
  this.div.dataset.markerId = this.marker.id
  const panes = this.getPanes()

  this.infoWindow = new InfoWindow({
    content: renderToStaticMarkup(React.createElement(Tooltip, {...this.marker}))
  })

  // Update for Google API v3: overlayLayer doesn't accept mouse events anymore
  panes.overlayMouseTarget.appendChild(this.div)
}

ShopTag.prototype.draw = function() {
  const projection = this.getProjection()
  const div = this.div

  const point = projection.fromLatLngToDivPixel(this.position)

  div.style.left = (point.x - (div.offsetWidth / 2)) + 'px'
  div.style.top =  (point.y) + 'px'

  window.google.maps.event.addDomListener(div, 'click', (e) => {
    this.infoWindow.setPosition(this.position)
    this.infoWindow.open(this.map)
  })

}

export default ShopTag
