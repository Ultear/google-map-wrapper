import PropTypes from 'prop-types'

import {lifecycle, compose, withState} from 'recompose'
import {connect} from 'react-redux'

import {fetchShops} from './actions/shops'

import ShopsList from 'components/ShopsList'
import GoogleMap from 'components/GoogleMap'

import styles from './styles.css'

const App = ({shops = [], mapCenter, setMapCenter}) => {
  return (
    <div className={styles.App}>
      <ShopsList shops={shops} setCenter={setMapCenter} />
      <GoogleMap
        markers={shops}
        center={mapCenter}
      />
    </div>
  )
}

App.propTypes = {
  shops: PropTypes.array.isRequired,
  mapCenter: PropTypes.object.isRequired,
  setMapCenter: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  shops: state.shops.data
})

export default compose(
  connect(mapStateToProps, {fetchShops}),
  lifecycle({
    componentWillMount() {
      this.props.fetchShops()
    }
  }),
  withState('mapCenter', 'setMapCenter', {lat: 52.2351646, lng: 20.9942039})
)(App)
