import PropTypes from 'prop-types'
import styles from './ShopsList.css'
import {map} from 'ramda'
import {v4} from 'uuid'

import ListItem from './ListItem'

const ShopsList = ({shops = [], setCenter}) => (
  <ul className={styles.ShopsList}>
    {map(item => <ListItem key={v4()} {...item} setCenter={setCenter}/>, shops)}
  </ul>
)

ShopsList.propTypes = {
  shops: PropTypes.array.isRequired,
  setCenter: PropTypes.func.isRequired
}

export default ShopsList
