import PropTypes from 'prop-types'
import styles from './ListItem.css'

const ListItem = ({network, address, id, setCenter, latitude, longitude}) => {

  const onClick = () => {
    let node = document.querySelector(`[data-marker-id="${id}"]`)

    if(node && typeof node.click === 'function') {
      node.click()
    }

    setCenter(() => ({lat: Number(latitude), lng: Number(longitude)}))
  }

  return (
    <li onClick={onClick} className={styles.ShopItem}>
      <span className={styles.Network}>{network}</span>
      <span className={styles.Address}>{address}</span>
    </li>
  )

}

ListItem.propTypes = {
  network: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired, // uuid v4 string
  setCenter: PropTypes.func.isRequired,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired
}

export default ListItem
