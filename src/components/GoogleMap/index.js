import GoogleMap from './GoogleMap'
import Marker from './Marker'
import Tooltip from './Tooltip'
import ShopTag from './ShopTag'

export default GoogleMap

export {
  Marker,
  Tooltip,
  ShopTag
}
