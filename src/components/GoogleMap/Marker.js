import PropTypes from 'prop-types'
import styles from './styles.css'

const Marker = ({logo, address, network}) => {

  return (
    <div className={styles.Marker}>
      {logo && <img src={logo} alt="" />}
      {!logo && <h3>{network}</h3>}
    </div>
  )

}

Marker.propTypes = {
  logo: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  network: PropTypes.string.isRequired
}

export default Marker
