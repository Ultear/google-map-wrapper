import {getShops} from 'api/api'

export const REQUEST_SHOPS = 'REQUEST_SHOPS'
export const FETCH_SHOPS = 'FETCH_SHOPS'
export const RECEIVE_SHOPS = 'RECEIVE_SHOPS'

const requestShops = () => ({
  type: REQUEST_SHOPS
})

const receiveShops = shops => ({
  type: RECEIVE_SHOPS,
  payload: shops
})

export const fetchShops = () => dispatch => {
  dispatch(requestShops())
  return getShops().then(shops => {
    dispatch(receiveShops(shops))
  })
}
