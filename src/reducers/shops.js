import {RECEIVE_SHOPS, REQUEST_SHOPS} from 'actions/shops'

const initialState = {
  isFetching: false,
  data: []
}

const reducer = (state = initialState, action) => {

  switch(action.type) {
    case REQUEST_SHOPS:
      return Object.assign({}, state, {isFetching: true})
    case RECEIVE_SHOPS:
      return Object.assign({}, state, {isFetching: false, data: action.payload})
    default:
      return state
  }

}

export default reducer
