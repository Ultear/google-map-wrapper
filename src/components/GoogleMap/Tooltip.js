import PropTypes from 'prop-types'

import styles from './styles.css'
import {addIndex, map} from 'ramda'
import {v4} from 'uuid'

const indexedMap = addIndex(map)

const Tooltip = ({hours, logo, address, network}) => {

  const openHours = Object.entries(hours)
  const dayIndex = new Date().getDay()

  return (
    <div className={styles.Tooltip}>
      <header className={styles.Tooltip__Header}>
        {logo && <img src={logo} alt="" />} <h3>{network}</h3>
        <span className={styles.Tooltip__Address}>{address}</span>
      </header>

      <table className={styles.Tooltip__OpenHours}>
        <tbody>
          {indexedMap((item, index) => {
            const [weekDay, hours] = item
            return (
              <tr className={(dayIndex === index && styles.CurrentDay)} key={v4()}>
                <td>{weekDay}</td>
                <td>{hours}</td>
              </tr>
            )
          }, openHours)}
        </tbody>
      </table>
    </div>
  )
}

Tooltip.propTypes = {
  hours: PropTypes.object.isRequired,
  logo: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  network: PropTypes.string.isRequired
}

export default Tooltip
